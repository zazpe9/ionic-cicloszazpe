import { Component, OnInit } from '@angular/core';
import { IBike } from '../shared/bike';
import { BikeService } from '../shared/bike.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  bikes: IBike[]=[];
  haveValues:boolean = false;
  constructor(private bikeService: BikeService, private route: Router) { }

  ngOnInit() {
    this.fetchData();
  }

  ionViewDidEnter(){
    this.fetchData()
    this.haveValues = true;
  }

  fetchData(){
    this.bikes = [];
    this.bikeService.getBikes().subscribe(
      (data: IBike[]) => {
      this.bikes = data;
      }
     );
  }

  bikeTapped(bike) {
    this.route.navigate(['bikes', bike.id]);
  }
}
