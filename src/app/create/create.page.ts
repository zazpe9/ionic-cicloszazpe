import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { BikeService } from '../shared/bike.service';
import { ActivatedRoute,Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { IBike } from '../shared/bike';
import { HomePage } from '../home/home.page';

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {
  errorMessage: string;
  bikeId:number;
  bike: IBike;
  bikeForm: FormGroup;

  constructor(
    private activatedroute: ActivatedRoute,
    private router: Router,
    private bikeService: BikeService,
    public toastController: ToastController,
    private home:HomePage
  ) { }

  ngOnInit() {
    this.bikeForm = new FormGroup({
      name: new FormControl(''),
      brand: new FormControl(''),
      date: new FormControl(''),
      cover: new FormControl(''),
      description: new FormControl(''),
    });

    this.bikeId = parseInt(this.activatedroute.snapshot.params['bikeId']);
  }
  async onSubmit() {
    const toast = await this.toastController.create({
      header: 'Guardar bici',
      position: 'top',
      buttons: [
        {
          side: 'start',
          icon: 'save',
          text: 'ACEPTAR',
          handler: () => {
            this.saveBike();
          }
        }, {
          text: 'CANCELAR',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    toast.present();
  }
  saveBike(): void {
    if (this.bikeForm.valid) {
      if (this.bikeForm.dirty) {
        this.bike = this.bikeForm.value;
        this.bike.id = this.bikeId;
        
        this.bikeService.createBike(this.bike)
          .subscribe(
            () => this.onSaveComplete(),
            (error: any) => this.errorMessage = <any>error
          );
        
      } else {
        this.onSaveComplete();
      }
    } else {
      this.errorMessage = 'Please correct the validation errors.';
    }
  }

  onSaveComplete(): void {
    // Reset the form to clear the flags
    this.bikeForm.reset();
    this.home.fetchData();
    this.router.navigate(['']);
    
  }
}
