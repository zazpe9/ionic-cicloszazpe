import { Component, OnInit } from '@angular/core';
import { BikeService } from '../shared/bike.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { IBike } from '../shared/bike';
import { HomePage } from '../home/home.page';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  id:number;
  public bike: IBike;
  errorMessage: string;

  constructor(
    private activatedrouter: ActivatedRoute,
    private router: Router,
    private bikeService: BikeService,
    public toastController: ToastController,
    private home: HomePage
  ) { }

  ngOnInit() {
    this.id = this.activatedrouter.snapshot.params['bikeId'];
    this.bikeService.getBikeById(this.id).subscribe(
      (data: IBike) => this.bike = data
    );
  }

  editRecord():void {
    this.router.navigate(['/bikes', this.id, 'edit'])
  }
  async removeRecord(id) {
    const toast = await this.toastController.create({
      header: 'Eliminar bici',
      position: 'top',
      buttons: [
        {
          side: 'start',
          icon: 'delete',
          text: 'ACEPTAR',
          handler: () => {
            this.bikeService.deleteBike(id).subscribe(
              () => this.onSaveComplete(),
              (error: any) => this.errorMessage = <any>error
            );
            
          }
        }, {
          text: 'CANCELAR',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    toast.present();
  }

  onSaveComplete(): void {
    this.router.navigate(['']);
    this.home.fetchData();
  }
}
