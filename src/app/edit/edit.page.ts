import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { BikeService } from '../shared/bike.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { IBike } from '../shared/bike';
import { HomePageModule } from '../home/home.module';
import { HomePage } from '../home/home.page';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {
  bikeForm: FormGroup;
  errorMessage: string;
  id: number;
  public bike: IBike;
  constructor(
    private router: Router,
    private bikeService: BikeService,
    public toastController: ToastController,
    private activatedrouter: ActivatedRoute,
    private home : HomePage

  ) { }
  ngOnInit() {
    this.bikeForm = new FormGroup({
      name: new FormControl(''),
      brand: new FormControl(''),
      date: new FormControl(''),
      cover: new FormControl(''),
      description: new FormControl(''),
    });

    this.id = parseInt(this.activatedrouter.snapshot.params['id']);
    this.getProduct(this.id);
    
  }

  getProduct(id: number): void {
    this.bikeService.getBikeById(id)
      .subscribe(
        (bike: IBike) => this.displayBike(bike),
        (error: any) => this.errorMessage = <any>error
      );
  }

  displayBike(bike: IBike): void {
    if (this.bikeForm) {
      this.bikeForm.reset();
    }
    this.bike = bike;

    // Update the data on the form
    this.bikeForm.patchValue({
      name: this.bike.name,
      brand: this.bike.brand,
      date: this.bike.date,
      cover: this.bike.cover,
      description: this.bike.description,
    });
  }



  editBike():void {
    if (this.bikeForm.valid) {
      if (this.bikeForm.dirty) {
        this.bike = this.bikeForm.value;
        this.bike.id = this.id;
        this.bikeService.updateBike(this.bike)
        .subscribe(
          () => this.onSaveComplete(),
          (error: any) => this.errorMessage = <any>error
        );
      
        
      } else {
        this.onSaveComplete();
      }
    } else {
      this.errorMessage = 'Please correct the validation errors.';
    }
      }
  

  onSaveComplete(): void {
    // Reset the form to clear the flags
    this.bikeForm.reset();
    this.home.fetchData();
    this.router.navigate(['']);
    

    
  }
}

