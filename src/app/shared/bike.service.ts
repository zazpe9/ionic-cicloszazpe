import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

import { IBike } from './bike';

@Injectable({
    providedIn: 'root'
  })
  export class BikeService {
    private bikesUrl = 'api/bikes';
  
    constructor(private http: HttpClient) { }
  
    getBikes(): Observable<IBike[]> {
      return this.http.get<IBike[]>(this.bikesUrl)
        .pipe(
          tap(data => console.log(JSON.stringify(data))),
          catchError(this.handleError)
        );
    }
  
    getMaxBikeId(): Observable<IBike> {
      return this.http.get<IBike[]>(this.bikesUrl)
      .pipe(
        // Get max value from an array
        map(data => Math.max.apply(Math, data.map(function(o) { return o.id; }))   ),
        catchError(this.handleError)
      );
    }
  
    getBikeById(id: number): Observable<IBike> {
      const url = `${this.bikesUrl}/${id}`;
      return this.http.get<IBike>(url)
        .pipe(
          tap(data => console.log('getBike: ' + JSON.stringify(data))),
          catchError(this.handleError)
        );
    }
  
    createBike(bike: IBike): Observable<IBike> {
      const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      bike.id = null;
      return this.http.post<IBike>(this.bikesUrl, bike, { headers: headers })
        .pipe(
          tap(data => console.log('createBike: ' + JSON.stringify(data))),
          catchError(this.handleError)
        );
    }
  
    deleteBike(id: number): Observable<{}> {
      const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      const url = `${this.bikesUrl}/${id}`;
      return this.http.delete<IBike>(url, { headers: headers })
        .pipe(
          tap(data => console.log('deleteBike: ' + id)),
          catchError(this.handleError)
        );
    }
  
    updateBike(bike: IBike): Observable<IBike> {
      const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      const url = `${this.bikesUrl}/${bike.id}`;
      return this.http.put<IBike>(url, bike, { headers: headers })
        .pipe(
          tap(() => console.log('updateBike: ' + bike.id)),
          // Return the product on an update
          map(() => bike),
          catchError(this.handleError)
        );
    }
  
    private handleError(err) {
      // in a real world app, we may send the server to some remote logging infrastructure
      // instead of just logging it to the console
      let errorMessage: string;
      if (err.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        errorMessage = `An error occurred: ${err.error.message}`;
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        errorMessage = `Backend returned code ${err.status}: ${err.body.error}`;
      }
      console.error(err);
      return throwError(errorMessage);
    }
  
  }