import { InMemoryDbService } from 'angular-in-memory-web-api';

export class BikeData implements InMemoryDbService {

  createDb() {
    let bikes = [
    {
      "id": 1,
      "name": 'ALMA 29ER',
      "brand": 'ORBEA',
      "date": '2010',
      "cover": 'https://upload.wikimedia.org/wikipedia/commons/8/8a/Orbea_Alma_29er_H50_2.jpg',
      "description": "Bici para aquellos que buscan rutas de monte exigentes."
    },
    {
      "id": 2,
      "name": 'MIAMI PRO',
      "brand": 'BH',
      "date": '2018',
      "cover": 'https://www.bikelec.es/media/catalog/product/cache/1/image/1200x1020/9df78eab33525d08d6e5fb8d27136e95/l/o/london_lux_wave.jpg',
      "description": "Bicicleta ideal para paseos por la ciudad."
    },
    {
      "id": 3,
      "name": 'WRC TSR3',
      "brand": 'CONOR',
      "date": '2017',
      "cover": 'https://www.bicimarket.com/files/product/1748auto_CONOR_WRC_TSR_3_ULTEGRA.jpeg',
      "description": "Bici de carretera profesional."
    }
    ];
    return { bikes: bikes };
  }
}