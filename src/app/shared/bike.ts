export interface IBike {
    id: number;
    name: string;
    brand: string;
    date: string;
    cover: string;
    description: string;
   }